#!/bin/sh
set -e

PATH=/opt/sbin:/opt/bin:$PATH
export PATH
DJANGO_FREERADIUS_API_TOKEN=${DJANGO_FREERADIUS_API_TOKEN}
export DJANGO_FREERADIUS_API_TOKEN

until ping -c 1 -W 1 db; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

until ping -c 1 -W 1 web; do
  >&2 echo "Django is unavailable - sleeping"
  sleep 1
done

until nc -z web 8000; do
  >&2 echo "Rest is unavailable - sleeping"
  sleep 1
done
# this if will check if the first argument is a flag
# but only works if all arguments require a hyphenated flag
# -v; -SL; -f arg; etc will work, but not arg1 arg2
if [ "$#" -eq 0 ] || [ "${1#-}" != "$1" ]; then
    set -- radiusd "$@"
fi

# check for the expected command
if [ "$1" = 'radiusd' ]; then
    shift
    exec radiusd -f  "$@"
fi

# debian people are likely to call "freeradius" as well, so allow that
if [ "$1" = 'freeradius' ]; then
    shift
    exec radiusd -f  "$@"
fi

# else default to run whatever the user wanted like "bash" or "sh"
exec "$@"
