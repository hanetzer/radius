#!/bin/sh
set -e

source /venv/bin/activate

# export DJANGO_SETTINGS_MODULE=${DJANGO_SETTINGS_MODULE}

until ping -c 1 -W 1 db; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

# if [ "x$DJANGO_MANAGEPY_MAKEMIGRATIONS" = 'xon' ]; then
#   /venv/bin/python manage.py makemigrations
# fi

if [ "x$DJANGO_MANAGEPY_MIGRATE" = 'xon' ]; then
  /venv/bin/python manage.py migrate --settings $DJANGO_SETTINGS_MODULE --noinput
fi

if [ "x$DJANGO_MANAGEPY_COLLECTSTATIC" = 'xon' ]; then
  /venv/bin/python manage.py collectstatic --noinput
fi

exec "$@"
