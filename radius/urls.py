from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    # stock admin url
    path('admin/', admin.site.urls),
    # django-freeradius urls (api and admin)
    path('', include('django_freeradius.urls', namespace='freeradius')),
    # rest-auth urls (login and registration)
    path('api/v1/rest-auth/', include('rest_auth.urls')),
    path('api/v1/registration/', include('rest_auth.registration.urls')),
]
